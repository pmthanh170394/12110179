﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog2_v2.Models
{
    [Table("Tag")]
    public class Tag
    {
        
        public int ID { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [MinLength(10, ErrorMessage = "Số lượng kí tự tối thiểu là 10 kí tự")]
        [MaxLength(100,ErrorMessage="Số lượng kí tự tối đa là 100 kí tự")]
        public string Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}