namespace Blog2_v2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan16 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Comment", "Title");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comment", "Title", c => c.String(nullable: false, maxLength: 500));
        }
    }
}
