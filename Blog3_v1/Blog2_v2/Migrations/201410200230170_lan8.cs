namespace Blog2_v2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan8 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Post", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Post", "Body", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Comment", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Comment", "Body", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Comment", "Author", c => c.String(nullable: false));
            AlterColumn("dbo.Tag", "Content", c => c.String(maxLength: 100));
            AlterColumn("dbo.Account", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Account", "FirstName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Account", "LastName", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Account", "LastName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Account", "FirstName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Account", "Email", c => c.String());
            AlterColumn("dbo.Tag", "Content", c => c.String());
            AlterColumn("dbo.Comment", "Author", c => c.String());
            AlterColumn("dbo.Comment", "Body", c => c.String());
            AlterColumn("dbo.Comment", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Post", "Body", c => c.String(maxLength: 100));
            AlterColumn("dbo.Post", "Title", c => c.String(nullable: false));
        }
    }
}
