﻿MSSV:12110156- Họ tên: Ngô Thắng Quốc
Trong thực tế cuộc sống của tôi, thời gian học tập chiếm phần lớn thời gian, tôi đã có một thời gian biểu cho riêng
mình nhưng chế độ ăn uống không hợp lí (quá nhiều chất béo và tinh bột) làm cho cơ thể tôi ngày càng mập lên và to ra,
nhưng cuộc sống luôn tôn sùng cái đẹp, vì thế tôi cũng muốn cải thiện vóc dáng của mình sao cho dễ nhìn nhất, tôi có 
thể tìm kiếm trên Google nhưng rất mất thời gian và nhiều nguồn thông tin khác nhau, vì vậy tôi muốn tìm kiếm cho mình
một Website có thể cung cấp cho mình những thứ mình cần như là: các thông tin về chỉ số cơ thể, khẩu phần ăn uống hợp
lí, chế độ luyện tập thể dục thích hợp...v..v.
Từ sự giới thiệu của bạn, tôi cảm thấy rằng sự lựa chọn đối với Website của bạn là đúng đắn.
1. Từ việc lấy dữ liệu của người dùng Website của bạn đã tính toán (dựa trên chỉ số BMI) và đưa ra giải pháp tối ưu 
để cải thiện vóc dáng.
2. Từ việc đưa ra giải pháp, Website đưa ra pháp đồ cần thiết để cải thiện vóc dáng của từng người riêng biệt
- Về chế độ ăn uống: Tùy thuộc vào từng người dùng khác nhau mà có những chế độ ăn uống riêng, đồng thời cung cấp các
thực phẩm bổ sung để đảm bảo sức khỏe.
- Về chế độ luyện tập: Dựa trên từng pháp đồ mà đưa ra thời gian luyện tập hợp lí, tránh tình trạng tập luyện quá sức
ảnh hưởng tới sức khỏe.
3. Website của bạn có diễn đàn để giao lưu trực tuyến giữa các người dùng là điểm tôi thích nhất, vì tôi luyện tập theo
pháp đồ để có một vóc dáng dễ nhìn hơn cũng vì một mục đích là giảm sự tự ti khi hòa nhập cùng cộng đồng, thế nên sự 
giao lưu này là một điểm mạnh.
Về phần giao diện Website tôi cảm thấy khá tốt cung cấp đầy đủ thông tin cho người dùng, đồng thời bảo vệ được thông tin
cá nhân của người dùng là rất tốt, vì thực tế cũng có câu nói rằng "Tốt khoe, xấu che".
Đó là những yếu tố để tôi quyết định trở thành người dùng của Website này.

