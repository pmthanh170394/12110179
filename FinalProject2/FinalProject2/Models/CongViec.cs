﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject2.Models
{
    public class CongViec
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [MinLength(5, ErrorMessage = "Số lượng kí tự tối thiểu là 5 kí tự")]
        [MaxLength(100, ErrorMessage = "Số lượng kí tự tối đa là 100 kí tự")]
        public string TenCongViec { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.Time)]
        public DateTime ThoiGianBatDau { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.Time)]
        public DateTime ThoiGianKetThuc { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.DateTime)]
        public DateTime Ngay { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        public string DiaDiem { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        public string MucDo { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        public string MoTa { set; get; }
        public string ThanhVienThamGia { set; get; }

        public int UserProfileUserId { set; get; }
        public virtual UserProfile UserProfile { set; get; } 
    }
}