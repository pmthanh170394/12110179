﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject2.Models
{
    public class KetBan
    {
        public int ID { set; get; }
        public string ID_NguoiKetBan { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [MinLength(10, ErrorMessage = "Số lượng kí tự tối thiểu là 10 kí tự")]
        [MaxLength(500, ErrorMessage = "Số lượng kí tự tối đa là 500 kí tự")]
        public string NoiDung { set; get; }

        public int UserProfileId { set; get; }
        public virtual UserProfile UserProfile { set; get; }
    }
}