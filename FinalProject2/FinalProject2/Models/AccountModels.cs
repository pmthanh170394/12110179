﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace FinalProject2.Models
{
    public class FinalProjectDbContext : DbContext
    {
        public FinalProjectDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<BanBe> BanBes { set; get; }
        public DbSet<KetBan> KetBans { set; get; }
        public DbSet<ThamGiaCongViec> ThamGiaCongViecs { set; get; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UserProfile>().HasMany(d => d.TroGiups).WithMany(p => p.UserProfiles).Map(t => t.MapLeftKey("UserProfileID").MapRightKey("TroGiupID").ToTable("TroGiup_UserProfile"));
        }
        public DbSet<CongViec> CongViecs { set; get; }
        public DbSet<TroGiup> TroGiup { set; get; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string HoTen { set; get; }
        public string GioiTinh { set; get; }
        public string DiaChi { set; get; }
        public string SoThich { set; get; }
        public string CongViec { set; get; }
        public DateTime NgaySinh { get; set; }
        public string Email { get; set; }
        public string ThoiGianTapTrungCongViec { set; get; }
        public bool Online { set; get; }
        public int Level { set; get; }
        public int LuongTruyCap { set; get; }

        public virtual ICollection<BanBe> BanBes { set; get; }

        public virtual ICollection<KetBan> KetBans { set; get; }

        public virtual ICollection<ThamGiaCongViec> ThamGiaCongViecs { set; get; }

        public virtual ICollection<CongViec> CongViecs { set; get; }

        public virtual ICollection<TroGiup> TroGiups { set; get; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string HoTen { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.DateTime)]
        public DateTime NgaySinh { set; get; }
       

        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Nhập sai dữ liệu rồi!")]
        public string Email { get; set; }

        public bool Online { set; get; }
        public int Level { set; get; }
        public int LuongTruyCap { set; get; }

    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
