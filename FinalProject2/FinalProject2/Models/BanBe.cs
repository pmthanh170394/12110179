﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject2.Models
{
    public class BanBe
    {
        public int ID { set; get; }
        public int ID_BanBe { set; get; }

        public int UserProfileId { set; get; }
        public virtual UserProfile UserProfile { set; get; }

        public int ThamGiaCongViecID { set; get; }
        public virtual ThamGiaCongViec ThamGiaCongViec { set; get; }
    }
}