namespace FinalProject2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        HoTen = c.String(),
                        GioiTinh = c.String(),
                        DiaChi = c.String(),
                        SoThich = c.String(),
                        CongViec = c.String(),
                        NgaySinh = c.DateTime(nullable: false),
                        Email = c.String(),
                        ThoiGianTapTrungCongViec = c.String(),
                        Online = c.Boolean(nullable: false),
                        Level = c.Int(nullable: false),
                        LuongTruyCap = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.BanBes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ID_BanBe = c.Int(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                        ThamGiaCongViecID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .ForeignKey("dbo.ThamGiaCongViecs", t => t.ThamGiaCongViecID, cascadeDelete: true)
                .Index(t => t.UserProfile_UserId)
                .Index(t => t.ThamGiaCongViecID);
            
            CreateTable(
                "dbo.ThamGiaCongViecs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ID_NguoiYeuCau = c.Int(nullable: false),
                        NoiDung = c.String(nullable: false, maxLength: 500),
                        UserProfileId = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.KetBans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ID_NguoiKetBan = c.String(),
                        NoiDung = c.String(nullable: false, maxLength: 500),
                        UserProfileId = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.CongViecs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenCongViec = c.String(nullable: false, maxLength: 100),
                        ThoiGianBatDau = c.DateTime(nullable: false),
                        ThoiGianKetThuc = c.DateTime(nullable: false),
                        Ngay = c.DateTime(nullable: false),
                        DiaDiem = c.String(nullable: false),
                        MucDo = c.String(nullable: false),
                        MoTa = c.String(nullable: false),
                        ThanhVienThamGia = c.String(),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.TroGiups",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ID_NguoiTroGiup = c.Int(nullable: false),
                        NoiDung = c.String(nullable: false, maxLength: 500),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TroGiup_UserProfile",
                c => new
                    {
                        UserProfileID = c.Int(nullable: false),
                        TroGiupID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserProfileID, t.TroGiupID })
                .ForeignKey("dbo.UserProfile", t => t.UserProfileID, cascadeDelete: true)
                .ForeignKey("dbo.TroGiups", t => t.TroGiupID, cascadeDelete: true)
                .Index(t => t.UserProfileID)
                .Index(t => t.TroGiupID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TroGiup_UserProfile", new[] { "TroGiupID" });
            DropIndex("dbo.TroGiup_UserProfile", new[] { "UserProfileID" });
            DropIndex("dbo.CongViecs", new[] { "UserProfileUserId" });
            DropIndex("dbo.KetBans", new[] { "UserProfile_UserId" });
            DropIndex("dbo.ThamGiaCongViecs", new[] { "UserProfile_UserId" });
            DropIndex("dbo.BanBes", new[] { "ThamGiaCongViecID" });
            DropIndex("dbo.BanBes", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.TroGiup_UserProfile", "TroGiupID", "dbo.TroGiups");
            DropForeignKey("dbo.TroGiup_UserProfile", "UserProfileID", "dbo.UserProfile");
            DropForeignKey("dbo.CongViecs", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.KetBans", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.ThamGiaCongViecs", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.BanBes", "ThamGiaCongViecID", "dbo.ThamGiaCongViecs");
            DropForeignKey("dbo.BanBes", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.TroGiup_UserProfile");
            DropTable("dbo.TroGiups");
            DropTable("dbo.CongViecs");
            DropTable("dbo.KetBans");
            DropTable("dbo.ThamGiaCongViecs");
            DropTable("dbo.BanBes");
            DropTable("dbo.UserProfile");
        }
    }
}
