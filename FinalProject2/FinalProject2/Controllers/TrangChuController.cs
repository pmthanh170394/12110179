﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject2.Models;
using System.Data.Entity;
namespace FinalProject2.Controllers
{
    [Authorize]
    public class TrangChuController : Controller
    {
        //
        // GET: /TrangChu/
        FinalProjectDbContext db = new FinalProjectDbContext(); 
        public ActionResult Index()
        {
            int iduser = db.UserProfiles.Where(m => m.UserName == User.Identity.Name).Single().UserId;
            return View(db.UserProfiles.Find(iduser));
        }

    }
}
