﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject2.Models;

namespace FinalProject2.Controllers
{
    [Authorize]
    public class CongViecController : Controller
    {
        private FinalProjectDbContext db = new FinalProjectDbContext();

        //
        // GET: /CongViec/

        public ActionResult Index()
        {
            return View(db.CongViecs.ToList());
        }

        //
        // GET: /CongViec/Details/5

        public ActionResult Details(int id = 0)
        {
            CongViec congviec = db.CongViecs.Find(id);
            if (congviec == null)
            {
                return HttpNotFound();
            }
            return View(congviec);
        }

        //
        // GET: /CongViec/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CongViec/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CongViec congviec)
        {
            if (ModelState.IsValid)
            {
                int iduser = db.UserProfiles.Where(m => m.UserName == User.Identity.Name).Single().UserId;
                congviec.UserProfileUserId = iduser;
    
                db.CongViecs.Add(congviec);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(congviec);
        }

        //
        // GET: /CongViec/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CongViec congviec = db.CongViecs.Find(id);
            if (congviec == null)
            {
                return HttpNotFound();
            }
            return View(congviec);
        }

        //
        // POST: /CongViec/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CongViec congviec)
        {
            if (ModelState.IsValid)
            {
                db.Entry(congviec).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(congviec);
        }

        //
        // GET: /CongViec/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CongViec congviec = db.CongViecs.Find(id);
            if (congviec == null)
            {
                return HttpNotFound();
            }
            return View(congviec);
        }

        //
        // POST: /CongViec/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CongViec congviec = db.CongViecs.Find(id);
            db.CongViecs.Remove(congviec);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}