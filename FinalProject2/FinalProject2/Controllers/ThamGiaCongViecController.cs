﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject2.Models;

namespace FinalProject2.Controllers
{
    [Authorize]
    public class ThamGiaCongViecController : Controller
    {
        private FinalProjectDbContext db = new FinalProjectDbContext();

        //
        // GET: /ThamGiaCongViec/

        public ActionResult Index()
        {
            return View(db.ThamGiaCongViecs.ToList());
        }

        //
        // GET: /ThamGiaCongViec/Details/5

        public ActionResult Details(int id = 0)
        {
            ThamGiaCongViec thamgiacongviec = db.ThamGiaCongViecs.Find(id);
            if (thamgiacongviec == null)
            {
                return HttpNotFound();
            }
            return View(thamgiacongviec);
        }

        //
        // GET: /ThamGiaCongViec/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ThamGiaCongViec/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ThamGiaCongViec thamgiacongviec)
        {
            if (ModelState.IsValid)
            {
                db.ThamGiaCongViecs.Add(thamgiacongviec);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(thamgiacongviec);
        }

        //
        // GET: /ThamGiaCongViec/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ThamGiaCongViec thamgiacongviec = db.ThamGiaCongViecs.Find(id);
            if (thamgiacongviec == null)
            {
                return HttpNotFound();
            }
            return View(thamgiacongviec);
        }

        //
        // POST: /ThamGiaCongViec/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ThamGiaCongViec thamgiacongviec)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thamgiacongviec).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(thamgiacongviec);
        }

        //
        // GET: /ThamGiaCongViec/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ThamGiaCongViec thamgiacongviec = db.ThamGiaCongViecs.Find(id);
            if (thamgiacongviec == null)
            {
                return HttpNotFound();
            }
            return View(thamgiacongviec);
        }

        //
        // POST: /ThamGiaCongViec/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ThamGiaCongViec thamgiacongviec = db.ThamGiaCongViecs.Find(id);
            db.ThamGiaCongViecs.Remove(thamgiacongviec);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}