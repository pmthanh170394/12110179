﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject2.Models;

namespace FinalProject2.Controllers
{
    [Authorize]
    public class BanBeController : Controller
    {
        private FinalProjectDbContext db = new FinalProjectDbContext();

        //
        // GET: /BanBe/

        public ActionResult Index()
        {
            var banbes = db.BanBes.Include(b => b.ThamGiaCongViec);
            return View(banbes.ToList());
        }

        //
        // GET: /BanBe/Details/5

        public ActionResult Details(int id = 0)
        {
            BanBe banbe = db.BanBes.Find(id);
            if (banbe == null)
            {
                return HttpNotFound();
            }
            return View(banbe);
        }

        //
        // GET: /BanBe/Create

        public ActionResult Create()
        {
            ViewBag.ThamGiaCongViecID = new SelectList(db.ThamGiaCongViecs, "ID", "NoiDung");
            return View();
        }

        //
        // POST: /BanBe/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BanBe banbe)
        {
            if (ModelState.IsValid)
            {
                db.BanBes.Add(banbe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ThamGiaCongViecID = new SelectList(db.ThamGiaCongViecs, "ID", "NoiDung", banbe.ThamGiaCongViecID);
            return View(banbe);
        }

        //
        // GET: /BanBe/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BanBe banbe = db.BanBes.Find(id);
            if (banbe == null)
            {
                return HttpNotFound();
            }
            ViewBag.ThamGiaCongViecID = new SelectList(db.ThamGiaCongViecs, "ID", "NoiDung", banbe.ThamGiaCongViecID);
            return View(banbe);
        }

        //
        // POST: /BanBe/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BanBe banbe)
        {
            if (ModelState.IsValid)
            {
                db.Entry(banbe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ThamGiaCongViecID = new SelectList(db.ThamGiaCongViecs, "ID", "NoiDung", banbe.ThamGiaCongViecID);
            return View(banbe);
        }

        //
        // GET: /BanBe/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BanBe banbe = db.BanBes.Find(id);
            if (banbe == null)
            {
                return HttpNotFound();
            }
            return View(banbe);
        }

        //
        // POST: /BanBe/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BanBe banbe = db.BanBes.Find(id);
            db.BanBes.Remove(banbe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}