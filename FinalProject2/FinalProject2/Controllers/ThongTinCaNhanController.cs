﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject2.Models;
using System.Data.Entity;
using System.Data;

namespace FinalProject2.Controllers
{
    [Authorize]
    public class ThongTinCaNhanController : Controller
    {
        //
        // GET: /ThongTinCaNhan/

       private  FinalProjectDbContext db = new FinalProjectDbContext();
       public ActionResult Index()
       {
           return View(db.UserProfiles.ToList());
       }
        //
        // GET: /ThongTinCaNhan/Details/5

        public ActionResult Details(int id = 0)
        {
           UserProfile thongtin = db.UserProfiles.Find(id);
            if (thongtin == null)
            {
                return HttpNotFound();
            }
            return View(thongtin);
        }

        //
        // GET: /TroGiup/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TroGiup/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserProfile thongtin)
        {
            if (ModelState.IsValid)
            {
                db.UserProfiles.Add(thongtin);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(thongtin);
        }

        //
        // GET: /TroGiup/Edit/5

        public ActionResult Edit(int id = 0)
        {
            UserProfile thongtin = db.UserProfiles.Find(id);
            if (thongtin == null)
            {
                return HttpNotFound();
            }
            return View(thongtin);
        }

        //
        // POST: /TroGiup/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserProfile thongtin)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thongtin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(thongtin);
        }

        //
        // GET: /TroGiup/Delete/5

        public ActionResult Delete(int id = 0)
        {
            UserProfile thongtin = db.UserProfiles.Find(id);
            if (thongtin == null)
            {
                return HttpNotFound();
            }
            return View(thongtin);
        }

        //
        // POST: /TroGiup/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserProfile thongtin = db.UserProfiles.Find(id);
            db.UserProfiles.Remove(thongtin);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
     }
   
  
}
