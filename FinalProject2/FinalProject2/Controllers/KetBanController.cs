﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject2.Models;

namespace FinalProject2.Controllers
{
    [Authorize]
    public class KetBanController : Controller
    {
        private FinalProjectDbContext db = new FinalProjectDbContext();

        //
        // GET: /KetBan/

        public ActionResult Index()
        {
            return View(db.KetBans.ToList());
        }

        //
        // GET: /KetBan/Details/5

        public ActionResult Details(int id = 0)
        {
            KetBan ketban = db.KetBans.Find(id);
            if (ketban == null)
            {
                return HttpNotFound();
            }
            return View(ketban);
        }

        //
        // GET: /KetBan/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /KetBan/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(KetBan ketban)
        {
            if (ModelState.IsValid)
            {
                db.KetBans.Add(ketban);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ketban);
        }

        //
        // GET: /KetBan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            KetBan ketban = db.KetBans.Find(id);
            if (ketban == null)
            {
                return HttpNotFound();
            }
            return View(ketban);
        }

        //
        // POST: /KetBan/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(KetBan ketban)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ketban).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ketban);
        }

        //
        // GET: /KetBan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            KetBan ketban = db.KetBans.Find(id);
            if (ketban == null)
            {
                return HttpNotFound();
            }
            return View(ketban);
        }

        //
        // POST: /KetBan/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KetBan ketban = db.KetBans.Find(id);
            db.KetBans.Remove(ketban);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}