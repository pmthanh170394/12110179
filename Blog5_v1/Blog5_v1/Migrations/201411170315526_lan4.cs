namespace Blog4_v1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tag",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Post", "Tag_ID", c => c.Int());
            AddForeignKey("dbo.Post", "Tag_ID", "dbo.Tag", "ID");
            CreateIndex("dbo.Post", "Tag_ID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Post", new[] { "Tag_ID" });
            DropForeignKey("dbo.Post", "Tag_ID", "dbo.Tag");
            DropColumn("dbo.Post", "Tag_ID");
            DropTable("dbo.Tag");
        }
    }
}
