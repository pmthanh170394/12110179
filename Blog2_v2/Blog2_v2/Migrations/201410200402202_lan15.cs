namespace Blog2_v2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan15 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tag", "Content", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tag", "Content", c => c.String(maxLength: 100));
        }
    }
}
