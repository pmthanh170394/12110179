namespace Blog2_v2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Posts", newName: "Post");
            RenameTable(name: "dbo.Comments", newName: "Comment");
            RenameTable(name: "dbo.Tags", newName: "Tag");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Tag", newName: "Tags");
            RenameTable(name: "dbo.Comment", newName: "Comments");
            RenameTable(name: "dbo.Post", newName: "Posts");
        }
    }
}
