namespace Blog2_v2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan10 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Post", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Post", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Comment", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Comment", "Body", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comment", "Body", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Comment", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Post", "Body", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Post", "Title", c => c.String(nullable: false, maxLength: 500));
        }
    }
}
