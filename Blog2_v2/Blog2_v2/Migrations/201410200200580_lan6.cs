namespace Blog2_v2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Post", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Post", "Body", c => c.String(maxLength: 100));
            AlterColumn("dbo.Comment", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Account", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Account", "FirstName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Account", "LastName", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Account", "LastName", c => c.String());
            AlterColumn("dbo.Account", "FirstName", c => c.String());
            AlterColumn("dbo.Account", "Password", c => c.String());
            AlterColumn("dbo.Comment", "Title", c => c.String());
            AlterColumn("dbo.Post", "Body", c => c.String());
            AlterColumn("dbo.Post", "Title", c => c.String());
        }
    }
}
