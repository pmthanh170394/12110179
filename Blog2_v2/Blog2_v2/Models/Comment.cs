﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog2_v2.Models
{
    [Table("Comment")]
    public class Comment
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [MinLength(20, ErrorMessage = "Số lượng kí tự tối thiểu là 20 kí tự")]
        [MaxLength(500, ErrorMessage = "Số lượng kí tự tối đa là 500 kí tự")]
        public string Title { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [MinLength(50, ErrorMessage = "Số lượng kí tự tối thiểu là 50 kí tự")]
        public string Body { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        public string Author { set; get; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}