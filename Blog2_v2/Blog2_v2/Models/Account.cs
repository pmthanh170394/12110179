﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog2_v2.Models
{
    [Table("Account")]
    public class Account
    {
        [Required(ErrorMessage="Không được bỏ trống")]
        [DataType(DataType.Password)]
        public string Password { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.EmailAddress,ErrorMessage="Nhap sai du lieu")]
        public string Email { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(100,ErrorMessage="Số lượng kí tự tối đa là 100")]
        public string FirstName { set; get;}
        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Số lượng kí tự tối đa là 100")]
        public string LastName { set; get; }
        public int AccountID { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}