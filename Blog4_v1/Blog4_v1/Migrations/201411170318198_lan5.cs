namespace Blog4_v1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Post", "Tag_ID", "dbo.Tag");
            DropIndex("dbo.Post", new[] { "Tag_ID" });
            CreateTable(
                "dbo.Tag_Post",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagID })
                .ForeignKey("dbo.Post", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tag", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TagID);
            
            DropColumn("dbo.Post", "Tag_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Post", "Tag_ID", c => c.Int());
            DropIndex("dbo.Tag_Post", new[] { "TagID" });
            DropIndex("dbo.Tag_Post", new[] { "PostID" });
            DropForeignKey("dbo.Tag_Post", "TagID", "dbo.Tag");
            DropForeignKey("dbo.Tag_Post", "PostID", "dbo.Post");
            DropTable("dbo.Tag_Post");
            CreateIndex("dbo.Post", "Tag_ID");
            AddForeignKey("dbo.Post", "Tag_ID", "dbo.Tag", "ID");
        }
    }
}
